import subprocess 

def exec(cmd, log=False):
    if log:
        print("Executing \"{}\"...".format(cmd))

    result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    if log:
        print("Output:\n{}".format(result.stdout.decode('utf-8')))
        print("Error:\n{}".format(result.stderr.decode('utf-8')))

    return result

