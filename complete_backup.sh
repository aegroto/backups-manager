echo "--- Creating backup files"
python3 create_backup.py

echo "--- Pushing backup"
python3 push_backup.py

echo "--- Clearing local files"
./clear_backups.sh
