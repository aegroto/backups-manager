import yaml, datetime

import shell_utils as shell

def compress_folder(name, dest):
    pass

def main():
    log = False

    main_folder = "backups"

    folders = yaml.safe_load(open("folders.yaml", "r"))

    for folder_info in folders:
        timestamp = datetime.datetime.now().strftime("%d-%m-%Y_%H:%M:%S")

        backup_folder = "{}/{}".format(main_folder, folder_info["group"])
        backup_name = "{}_{}".format(folder_info["name"], timestamp)
        backup_file = "{}/{}".format(backup_folder, backup_name)

        print("Creating backup of {}...".format(backup_file))

        ###################
        ## Folders setup ##
        ###################

        print("Cloning original folder...")
        # shell.exec("cp -r {} backup_tmp/".format(folder_info["folder"]), log=log)
        excluded_options = ""

        for excluded_folder in folder_info["exclude"]:
            excluded_options += "--exclude {} ".format(excluded_folder)

        shell.exec("rsync -av --progress {} backup_tmp/ {}".format(folder_info["folder"], excluded_options), log=log)

        print("Clearing current backup...")
        shell.exec("rm -r {}".format(backup_folder), log=log)

        print("Creating parent directory...")
        shell.exec("mkdir {}".format(backup_folder), log=log)

        ########################
        ## Backup compression ##
        ########################

        print("Compressing new files...")
        shell.exec("tar --force-local -czvf {}.tar.gz backup_tmp/".format(backup_name), log=log)

        ###################
        ## Backup moving ##
        ###################

        print("Moving archive...")
        shell.exec("mv {}.tar.gz {}.tar.gz".format(backup_name, backup_file), log=log)

        print("Removing clone folder...")
        shell.exec("rm -r backup_tmp/", log=log)

if __name__ == "__main__":
    main()
