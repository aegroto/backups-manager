import os, drive_utils

def main():
    for subdir in os.listdir("backups"):
        drive_utils.upload_folder("backups/{}".format(subdir), True)

if __name__ == "__main__":
    main()
